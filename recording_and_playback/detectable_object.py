class DetectableObject:
    def __init__(self, object_id, point, max_points):
        self.object_id = object_id
        self.position_history = []
        self.max_points = max_points
        if point is not None:
            self.add_coordinate(point)
        self.classification = None

##    # setup hazard_detection() as an abstract method
##    @abstractmethod
##    def hazard_detection(self):
##        pass

    # getter for object_id
    @property
    def object_id(self):
        return self._object_id

    # setter for object_id
    @object_id.setter
    def object_id(self, object_id):
        self._object_id = object_id

    # getter for position history
    @property
    def position_history(self):
        return self._position_history

    # setter for position history
    @position_history.setter
    def position_history(self, position_history):
        self._position_history = position_history

    # helper function to add coordinates to position history
    def add_coordinate(self, point):
        self.position_history.append(point)
        if len(self.position_history) > self.max_points:
            self.position_history.pop(0)

    def get_last_saved_coordinate(self):
        return self.position_history[-1]

    def get_first_saved_coordinate(self):
        return self.position_history[0]
