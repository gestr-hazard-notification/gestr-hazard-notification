import math

import numpy as np


class TrajectoryEstimation:
    def __init__(self, collision_trajectory_threshold=1, collision_time_to_impact=4):
        self.collision_trajectory_threshold = collision_trajectory_threshold
        self.collision_time_to_impact = collision_time_to_impact

    def best_fit_slope_and_intercept(self, points):
        points = [item for item in points]
        xs = np.array([item[0] for item in points])
        zs = np.array([item[1] for item in points])

        m, b = np.polyfit(xs, zs, 1)

        return m, b

    def driving_towards_bike(self, points):
        try:
            # if self.is_ascending(points):
            #    return False
            m, b = self.best_fit_slope_and_intercept(points)
        except ValueError:
            return False
        bike_clearance = abs(b) / math.sqrt(math.pow(m, 2) + 1)

        return bike_clearance < self.collision_trajectory_threshold

    def is_impact_close(self, points):
        last, first = points[-1], points[0]
        x1, z1 = last['value']
        x2, z2 = first['value']
        lf_distance = math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
        if lf_distance == 0:
            return False
        timed = abs(first['timestamp'] - last['timestamp'])
        speed = lf_distance / timed  # m/s
        target_distance = math.sqrt(math.pow(x2, 2) + math.pow(z2, 2))
        tti = target_distance / speed

        return tti < self.collision_time_to_impact

    def is_ascending(self, points):
        last, first = points[-1], points[0]
        x1, z1 = last['value']
        x2, z2 = first['value']

        distance1 = math.sqrt(math.pow(x1, 2) + math.pow(z1, 2))
        distance2 = math.sqrt(math.pow(x2, 2) + math.pow(z2, 2))

        return distance1 > distance2
        
        



