# import argparse

import cv2
from cv_pipeline import MockupCNNPipeline
import time
import json
import numpy as np
import depthai
class DataEncoder(json.JSONEncoder):
    def default(self, obj):
        print(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, depthai.Detections):
            return [detection.get_dict() for detection in obj]
        return json.JSONEncoder.default(self, obj)

class CVSampler:
    def __init__(self):
        self.create_pipeline = MockupCNNPipeline
        self.p = None
        self.detections = []
        self.xz = {}
        self.current_time = 0
        self.shown_time = time.time()

        self.labels = [
                    "background",
                    "aeroplane",
                    "bicycle",
                    "bird",
                    "boat",
                    "bottle",
                    "bus",
                    "car",
                    "cat",
                    "chair",
                    "cow",
                    "diningtable",
                    "dog",
                    "horse",
                    "motorbike",
                    "person",
                    "pottedplant",
                    "sheep",
                    "sofa",
                    "train",
                    "tvmonitor"
                ]

        self.color_labels = [
                    None,               #"background",
                    None,               #"aeroplane",
                    (27, 231, 255),     #"bicycle",
                    None,               #"bird",
                    None,               #"boat",
                    None,               #"bottle",
                    (110, 235, 131),    #"bus",
                    (228, 255, 26),     #"car",
                    None,               #"cat",
                    None,               #"chair",
                    None,               #"cow",
                    None,               #"diningtable",
                    (232, 170, 20),     #"dog",
                    None,               #"horse",
                    (255, 87, 20),      #"motorbike",
                    (235, 110, 214),    #"person",
                    None,               #"pottedplant",
                    None,               #"sheep",
                    None,               #"sofa",
                    None,               #"train",
                    None,               #"tvmonitor"
                ]
    def show_bounding_boxes(self,frame, detections):

        frame_h = frame.shape[0]
        frame_w = frame.shape[1]

        x_detections = []
        z_detections = []
        label_detections = []
        for detection in detections:
            if self.color_labels[detection.label] != None:
                pt1 = int(detection.x_min * frame_w), int(detection.y_min * frame_h)
                pt2 = int(detection.x_max * frame_w), int(detection.y_max * frame_h)
                cv2.rectangle(frame, pt1, pt2, self.color_labels[detection.label], 2)

                x1, y1 = pt1
                x2, y2 = pt2

                cv2.rectangle(frame, pt1, pt2, self.color_labels[detection.label])
                # Handles case where TensorEntry object label is out if range
                if self.labels is not None:
                    label = self.labels[detection.label]
                else:
                    label = str(detection.label)

                pt_t1 = x1, y1 + 20
                cv2.putText(frame, label, pt_t1, cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.color_labels[detection.label], 2)

                pt_t2 = x1, y1 + 40
                cv2.putText(frame, '{:.2f}'.format(100*detection.confidence) + ' %', pt_t2, cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.color_labels[detection.label])

                pt_t3 = x1, y1 + 60
                cv2.putText(frame, 'x:' '{:7.3f}'.format(detection.depth_x) + ' m', pt_t3, cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.color_labels[detection.label])

                pt_t4 = x1, y1 + 80
                cv2.putText(frame, 'z:' '{:7.3f}'.format(detection.depth_z) + ' m', pt_t4, cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.color_labels[detection.label])

                x_detections.append(detection.depth_x)
                z_detections.append(detection.depth_z)
                label_detections.append(self.labels[detection.label])

        # time is current time, x is left/right distance,
        # z is depth, l are the objects labels, s is the source (0 for cv and 1 for radar)
        self.xz = {
            'time': time.time(),
            'x': x_detections,
            'y': z_detections,
            'l': label_detections,
            's': 0
        }

        return frame

    def cv_playback(self, to_master):
        self.p = self.create_pipeline(data_path="data", start_time=0, end_time=None)
        while True:
            nnet_packets, data_packets = self.p.get_available_nnet_and_data_packets()

            for nnet_packet in nnet_packets:
                self.detections = list(nnet_packet.getDetectedObjects())

            self.current_time = time.time()

            for packet in data_packets:
                if packet.stream_name == 'previewout':
                    data = packet.getData()
                    data0 = data[0, :, :]
                    data1 = data[1, :, :]
                    data2 = data[2, :, :]
                    frame = cv2.merge([data0, data1, data2])

                    img_h = frame.shape[0]
                    img_w = frame.shape[1]

                    frame = self.show_bounding_boxes(frame, self.detections)
                    self.shown_time = time.time()
                    imS = cv2.resize(frame, (480, 480))                    # Resize image
                    cv2.imshow("output", imS)
                    #print("CV type: ", type(self.xz))
                    to_master.send(self.xz)
            if(self.current_time - self.shown_time > 1):
                break
            if cv2.waitKey(1) == ord('q'):
                break
    def _stream_type(self, option):
        option_list = option.split(",")
        if len(option_list) > 2:
            raise ValueError(f"{option} format is invalid.")

        stream_name = option_list[0]
        if len(option_list) == 1:
            stream_dict = {"name": stream_name}
        else:
            try:
                max_fps = float(option_list[1])
            except ValueError:
                raise ValueError(f"In option: {option} {option_list[1]} is not a number!")

            stream_dict = {"name": stream_name, "max_fps": max_fps}
        return stream_dict


    def record_depthai_mockups(self, to_master, to_children, path):
        import argparse
        import depthai
        import time
        import cv2
        import csv
        import numpy as np
        from pathlib import Path
        from concurrent.futures.thread import ThreadPoolExecutor

        def __get_fullpath(path):
            return str((Path(__file__).parent / Path(path)).resolve().absolute())

        parser = argparse.ArgumentParser()

        parser.add_argument(
            "-s", "--streams",
            nargs="+",
            type=self._stream_type,
            dest="streams",
            default=["metaout", "previewout"],
            help="Define which streams to enable \n"
                "Format: stream_name or stream_name,max_fps \n"
                "Example: -s metaout previewout \n"
                "Example: -s metaout previewout,10 depth_sipp,10"
        )
        args = parser.parse_args()

        dest = Path(path).resolve().absolute()

        if dest.exists() and len(list(dest.glob('*'))) != 0:
            raise RuntimeError(
                f"Path {dest} contains {len(list(dest.glob('*')))} files. Either specify new path or remove files from this directory")
        dest.mkdir(parents=True, exist_ok=True)

        device = depthai.Device("", False)

        p = device.create_pipeline(config={'streams': ['metaout', 'previewout'], 'depth': {'calibration_file': '', 'left_mesh_file': '/Users/jonathonmcnabb/school/gestr/depthai/resources/mesh_left.calib', 'right_mesh_file': '/Users/jonathonmcnabb/school/gestr/depthai/resources/mesh_right.calib', 'padding_factor': 0.3, 'depth_limit_m': 10.0, 'median_kernel_size': 7, 'lr_check': False, 'warp_rectify': {'use_mesh': False, 'mirror_frame': True, 'edge_fill_color': 0}}, 'ai': {'blob_file': '/Users/jonathonmcnabb/school/gestr/depthai/resources/nn/mobilenet-ssd/mobilenet-ssd.blob.sh14cmx14NCE1', 'blob_file_config': '/Users/jonathonmcnabb/school/gestr/depthai/resources/nn/mobilenet-ssd/mobilenet-ssd.json', 'blob_file2': '', 'blob_file_config2': '', 'calc_dist_to_bb': True, 'keep_aspect_ratio': True, 'camera_input': 'rgb', 'shaves': 14, 'cmx_slices': 14, 'NN_engines': 1}, 'ot': {'max_tracklets': 20, 'confidence_threshold': 0.5}, 'board_config': {'swap_left_and_right_cameras': True, 'left_fov_deg': 71.86, 'rgb_fov_deg': 68.7938, 'left_to_right_distance_cm': 7.5, 'left_to_rgb_distance_cm': 3.75, 'store_to_eeprom': False, 'clear_eeprom': False, 'override_eeprom': False}, 'camera': {'rgb': {'resolution_h': 1080, 'fps': 30.0}, 'mono': {'resolution_h': 400, 'fps': 30.0}}, 'app': {'sync_video_meta_streams': False, 'sync_sequence_numbers': False, 'usb_chunk_KiB': 64}})

        if p is None:
            raise RuntimeError("Error initializing pipelne")

        start_ts = time.time()
        nnet_storage = []
        frames_storage = []

        print('Depthai version installed: ', depthai.__version__)

        with ThreadPoolExecutor(max_workers=100) as pool:
            while True:
                nnet_packets, data_packets = p.get_available_nnet_and_data_packets()
                for nnet_packet in nnet_packets:
                    try:
                        nnet_storage.append((time.time(), "nnet",nnet_packet.getDetectedObjects()))
                    except RuntimeError:
                        nnet_storage.append((time.time(), "nnet", nnet_packet.getOutputsDict()))

                for packet in data_packets:
                    frame = packet.getData()
                    if frame is not None:
                        frames_storage.append((time.time(), packet.stream_name, frame))

                        if packet.stream_name == 'previewout':
                            data = packet.getData()
                            data0 = data[0, :, :]
                            data1 = data[1, :, :]
                            data2 = data[2, :, :]
                            frame = cv2.merge([data0, data1, data2])
                            if(len(nnet_storage) > 0):
                                frame = self.show_bounding_boxes(frame, nnet_storage[-1][2])
                        imS = cv2.resize(frame, (960, 960))                    # Resize image
                        cv2.imshow("output", imS)

                if cv2.waitKey(1) == ord('q'):
                    break
                if to_children.poll(0):
                    if to_children.recv() == "SAVE":
                        print("SAVING CV")
                        break

            with open(dest / Path('dataset.tsv'), 'w') as out_file:
                for ts, source, data in sorted([*frames_storage, *nnet_storage], key=lambda item: item[0]):
                    filename = Path(f"{int((ts - start_ts) * 10000)}-{source}")
                    if source == "nnet":
                        filename = filename.with_suffix('.json')
                        with open(dest / filename, 'w') as f:
                            json.dump(data, f, cls=DataEncoder)
                    else:
                        filename = filename.with_suffix('.npy')
                        pool.submit(np.save, dest / filename, data)
                    tsv_writer = csv.writer(out_file, delimiter='\t')
                    tsv_writer.writerow([ts - start_ts, source, filename])

            to_master.send("Finished saving frames CV")