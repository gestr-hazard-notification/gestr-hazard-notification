from detectable_object import DetectableObject
from trajectory_estimation import TrajectoryEstimation
import numpy as np
import time as time

class ObjectManager:
	def __init__(self):
		self.current_object_id = 0
		self.detected_objects = []
		self.new_points = []
		self.comparison_radius_history = 2
		self.comparison_radius_new =  2
		self.max_stored_points = 400

		self.path_estimator = TrajectoryEstimation()

		self.classification_dict = {
            0: 'vehicle',
            1: 'bike',
            2: 'person',
        }

	def get_distance_between(self, point1, point2):
		new_point = []
		new_point.append(point2[0] - point1[0])
		new_point.append(point2[1] - point1[1])
		distance = np.sqrt(new_point[0]**2 + new_point[1]**2)
		return distance

	def average_point_array(self, point_array):
		x_sum = 0
		y_sum = 0
		for elem in point_array:
			x_sum += elem[0]
			y_sum += elem[1]

		x_sum /= len(point_array)
		y_sum /= len(point_array)

		return (x_sum, y_sum)

	def add_new_points(self, point):
		self.new_points.append(point)

	def group_new_points(self):
		new_new_points = []
		while(len(self.new_points) > 0):
			first_p = self.new_points[0]
			points_in_set = [first_p]
			amount_popped = 0
			for i in range(len(self.new_points)-1):
				if(self.get_distance_between(self.new_points[i+1-amount_popped], first_p) < self.comparison_radius_new):
					points_in_set.append(self.new_points[i+1-amount_popped])
					self.new_points.pop(i+1-amount_popped)
					amount_popped += 1
			averaged_point = self.average_point_array(points_in_set)
			self.new_points.pop(0)
			new_new_points.append(averaged_point)
		self.new_points = new_new_points

	def should_object_be_added_to_existing(self, point):
		detected_object_index = -1
		for i in range(len(self.detected_objects)):
			objects_last_points = self.detected_objects[i].get_last_saved_coordinate()
			distance_between_points = self.get_distance_between(point, objects_last_points)
			if(distance_between_points < self.comparison_radius_history):
				# return true if the point should be appeneded to an existing object
				detected_object_index = i
				break
		return detected_object_index

	def create_a_new_detectable_object(self, point):
		new_object = DetectableObject(self.current_object_id, point, self.max_stored_points)
		self.detected_objects.append(new_object)
		self.current_object_id += 1
		return

	def run_point_comparisons(self):
		# 1) loop through existing detected_objects and compare new points to data points
		for i in range(len(self.new_points)):
			detected_object_id = self.should_object_be_added_to_existing(self.new_points[i])
			if(detected_object_id > -1):
				# append point to existing object
				self.detected_objects[detected_object_id].add_coordinate(self.new_points[i])
			else:
				# create a new detetable object if the point does not align with any existing point history
				self.create_a_new_detectable_object(self.new_points[i])

	def get_last_point_for_all_objects(self):
		ids = []
		x = []
		y = []
		for obj in self.detected_objects:
			ids.append(obj.object_id)
			last_point = obj.get_last_saved_coordinate()
			x.append(last_point[0])
			y.append(last_point[1])
		return ids, x, y

	def get_path_for_object(self, object_id):
		points = self.detected_objects[object_id].position_history
		xs = []
		ys = []
		if len(points) > 20:
			m, b = self.path_estimator.best_fit_slope_and_intercept(points)
			first_point = self.detected_objects[object_id].get_first_saved_coordinate()
			xs = [-15, 15]
			ys = [m*xs[0]+b, m*xs[1]+b]
		return xs, ys

	def collision_will_happen(self, object_id):
		points = self.detected_objects[object_id].position_history
		if self.path_estimator.driving_towards_bike(points):
			return True

	def parse_message(self, json):
		for i in range(len(json['x'])):
			new_point = (json['x'][i], json['y'][i])
			self.add_new_points(new_point)
		self.group_new_points()
		self.run_point_comparisons()
		self.new_points = []


