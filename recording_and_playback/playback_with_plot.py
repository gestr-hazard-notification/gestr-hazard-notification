#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import pyqtgraph as pg
import pyqtgraph.multiprocess as mp
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets
from pyqtgraph.ptime import time
import pyqtgraph.parametertree as ptree
import pyqtgraph.graphicsItems.ScatterPlotItem
import time as python_time
from RadarSampler import RadarSampler
from CVSampler import CVSampler
import multiprocessing
from multiprocessing import current_process

data_parent_conn, data_child_conn = multiprocessing.Pipe()

from object_manager import ObjectManager

import random
import colorsys


# A custom ItemSample to draw in legend
from pyqtgraph.graphicsItems.LegendItem import ItemSample
from pyqtgraph.graphicsItems.ScatterPlotItem import drawSymbol

###########################
###### VISUALIZATION ######
###########################
pg.mkQApp()

x1 = 200
x2 = 0
y1 = 0
y2 = 0

if(current_process().name == 'RECEIVER'):
	proc = mp.QtProcess()
	rpg = proc._import('pyqtgraph')
	plotwin = rpg.plot()
	curve = plotwin.plot()
	regression = plotwin.plot()

	dotted_pen = rpg.mkPen('y', width=7, style=QtCore.Qt.DashDotLine)          ## Make a dashed yellow line 2px wide


	plotwin.setRange(xRange=[-30,30], yRange=[0,90])
	# adding legend
	legend = rpg.LegendItem((100,60), offset=(440,10), labelTextSize='15pt', pen='w')  # args are (size, offset)
	legend.setParentItem(plotwin.graphicsItem())


	# Crude patch of the legend paint() method
	import types
	def paint(self, p, *args):
		p.fillRect(self.boundingRect(), rpg.mkBrush(192,0,0,50))
	legend.paint = types.MethodType(paint, legend)

	# Fix the spacing between legend symbol and text.
	# This is probably needs to be added to the __init__() of LegendItem...
	legend.layout.setHorizontalSpacing(20)

	collision_text = rpg.TextItem(html='<div style="text-align: center"><span style="color: #FFF;">COLLISION!</span></div>', anchor=(-0.3,0.5), angle=0, border='w', fill=(255, 0, 0, 100))
	collision_text.setPos(0, -100)
	plotwin.addItem(collision_text)


	manager_pen = rpg.PlotDataItem(
            antialias=True,
            pen=None, symbol='d', symbolBrush=(0, 255, 0, 255))

	trajectory_pen = rpg.PlotDataItem(
            antialias=True,
            pen=dotted_pen, symbol=None, symbolBrush=None)
	legend.addItem(item=manager_pen,name="RADAR OBJECTS")
	#legend.addItem(item=trajectory_pen,name="estimated trajectory")

	id_text = rpg.TextItem(html='<div style="text-align: center"><span style="color: #FFF;">object_id</span><br><span style="color: #FF0; font-size: 16pt;">' + str(14) + '</span></div>', anchor=(-0.3,0.5), angle=0, border='w', fill=(0, 0, 255, 100))
	id_text.setPos(0, -100)
	plotwin.addItem(id_text)


	#data = proc.transfer([])

###############################
###### SAMPLING PLAYBACK ######
###############################


def data_receiver( conn):
	"""
	function to print the messages received from other
	end of pipe
	"""

	manager = ObjectManager()

	processed_packets = 0
	id_to_show = 14

	while True:
		msg = conn.recv()
		manager.parse_message(msg)
		i = -1
		ids, x, y = manager.get_last_point_for_all_objects()
		print(ids)
		print(x)
		print(y)
		curve.setData(x, y, _callSync='off', pen=None, symbol='d', symbolBrush=(0, 255, 0, 255), name="tracked points")
		# if processed_packets % 25 == 0:
		# 	try:
		# 		i = ids.index(id_to_show)
		# 	except:
		# 		pass
		# 	if i > -1:
		# 		xs, ys = manager.get_path_for_object(id_to_show)
		# 		regression.setData(xs, ys, _callSync='off', pen=dotted_pen, symbol=None, symbolBrush=None, name="tracked points")
		# 		id_text.setPos(x[i], y[i])

		# 		colission_imminent = manager.collision_will_happen(id_to_show)
		# 		if colission_imminent:
		# 			collision_text.setPos(15, 45)
		# 		else:
		# 			collision_text.setPos(0, -100)
		# processed_packets += 1


def get_radar(conn):
	RADAR = RadarSampler()
	RADAR.radar_playback(conn)
	return 0

def get_cv(conn):
	CV = CVSampler()
	CV.cv_playback(conn)
	return 0

def start_pipe():
	# creating new processes
	p1 = multiprocessing.Process(target=get_radar, args=(data_parent_conn,))
	p2 = multiprocessing.Process(target=get_cv, args=(data_parent_conn,))
	p3 = multiprocessing.Process(target=data_receiver, args=(data_child_conn,))

	p3.name = "RECEIVER"

	# running processes
	p1.start()
	p2.start()
	p3.start()

	# wait until processes finish
	p1.join()
	p2.join()
	p3.join()



# Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
	start_pipe()