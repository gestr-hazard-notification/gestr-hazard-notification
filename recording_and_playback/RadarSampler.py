from serial import Serial
import numpy as np
import time
from pathlib import Path
from radar_pipeline import MockupRadarPipeline



class RadarSampler:
    def __init__(self):
        try:
            self.s = Serial(port='/dev/tty.usbmodemR20910494', baudrate=921600)
        except:
            print("Radar port cannot be opened")

        self.OBJ_STRUCT_SIZE_BYTES = 10
        self.TRACKER_STRUCT_SIZE_BYTES = 12

        self.header = bytearray(b'\x02\x01\x04\x03\x06\x05\x08\x07')
        self.array_confirmation = []

        self.barker_count = 0
        self.debug = False

        self.idx = 0
        self.radar_storage = []
        self.start_ts = time.time()

        # playback pipeline
        self.create_pipeline = MockupRadarPipeline

    def tlv_switch(self,idx, byte_vec):
        idx, tlv_type, tlv_length = self.get_tlv(byte_vec, idx)
        found_x_points = []
        found_y_points = []
        found_x_tracked = []
        found_y_tracked = []
        if(tlv_type == 1):
            found_x_points, found_y_points, idx = self.parse_detected_points(byte_vec, idx, tlv_length)
        elif(tlv_type == 2):
            idx = self.parse_clusters(byte_vec, idx, tlv_length)
        elif(tlv_type == 3):
            found_x_tracked, found_y_tracked, idx = self.parse_tracked_object(byte_vec, idx, tlv_length)
        elif(tlv_type == 4):
            idx = self.parse_parking_assist(byte_vec, idx, tlv_length)
        elif(tlv_type == 5):
            idx = self.parse_stats(byte_vec, idx, tlv_length)
        else:
            if(self.debug):
                print("DEBUG: END OF PACKET")
            idx += 9999
            #print(tlv_type)

        return [found_x_points, found_y_points], [found_x_tracked, found_y_tracked], idx

    def get_tlv(self,byte_vec, idx):

        available_bytes = len(byte_vec) - idx - 1
        if(available_bytes < 4):
            return 9999, 9999, 0
        word = [1, 256, 65536, 16777216]

        tlv_type = byte_vec[idx] * word[0] + byte_vec[idx + 1] * word[1] + byte_vec[idx + 2] * word[2] + byte_vec[idx + 3] * word[3]
        idx += 4
        if(self.debug):
            print("TVL TYPE: ", tlv_type)
        available_bytes = len(byte_vec) - idx - 1
        if(available_bytes < 4):
            return 9999, 9999, 0
        word = [1, 256, 65536, 16777216]
        tlv_length = byte_vec[idx] * word[0] + byte_vec[idx + 1] * word[1] + byte_vec[idx + 2] * word[2] + byte_vec[idx + 3] * word[3]
        if(self.debug):
            print("TLV LENGTH: ", tlv_length)
        idx += 4
        return idx, tlv_type, tlv_length



    def parse_detected_points(self,byte_vec, idx, tlv_length):
        if tlv_length > 0:
            word = [1, 256]
            # get number of objects
            number_of_objects = byte_vec[idx] * word[0] + byte_vec[idx + 1] * word[1]
            if(self.debug):
                print("num objects: ", number_of_objects)
            idx += 2
            # get "code" for correctly parsing values
            xyzQ_format_sum = byte_vec[idx] * word[0] + byte_vec[idx + 1] * word[1]
            if(self.debug):
                print("format: ", xyzQ_format_sum)
            xyzQ_format = 2 ** xyzQ_format_sum
            idx += 2
            inv_xyzQ_format = 1 / xyzQ_format

            #object_bytes = byte_vec.slice(idx, number_of_objects*OBJ_STRUCT_SIZE_BYTES)
            #idx = idx + number_of_objects*OBJ_STRUCT_SIZE_BYTES

            x_arr = []
            y_arr = []
            z_arr = []
            doppler_arr = []
            object_range_arr =[]
            peak_value_arr = []

            # loop through all the objects
            for x in range(number_of_objects):

                # parse and convert values
                doppler = byte_vec[idx] + byte_vec[idx+1] * 256
                peak_value = byte_vec[idx+2] + byte_vec[idx+3] * 256

                x = byte_vec[idx+4] + byte_vec[idx+5] * 256
                y = byte_vec[idx+6] + byte_vec[idx+7] * 256
                z = byte_vec[idx+8] + byte_vec[idx+9] * 256

                if(x > 32767):
                    x = x - 65536

                if(y > 32767):
                    y = y - 65536

                if(z > 32767):
                    z = z - 65536

                if(doppler > 32767):
                    doppler = doppler - 65536

                x = x*inv_xyzQ_format
                y = y*inv_xyzQ_format
                z = -(z*inv_xyzQ_format)
                doppler = doppler*inv_xyzQ_format
                object_range = np.sqrt(y**2 + x**2 + z**2)

                # append values to array
                x_arr.append(x)
                y_arr.append(y)
                z_arr.append(z)

                doppler_arr.append(doppler)
                peak_value_arr.append(peak_value)

                # increase idx for next object
                idx += self.OBJ_STRUCT_SIZE_BYTES
            if(self.debug):
                print("X: ", x_arr)
                print("Y: ", y_arr)
                print("Z: ", z_arr)

        # if we processed the data, idx has been increased correctly
        # if tlv_length !> 0, idx will remain the same

        return x_arr, y_arr, idx


    def parse_clusters(self,byte_vec, idx, tlv_length):
        idx = idx + tlv_length
        return idx

    def parse_tracked_object(self,byte_vec, idx, tlv_length):
        #print("TRACKED")
        if tlv_length > 0:
            word = [1, 256]
            number_of_objects = byte_vec[idx] * word[0] + byte_vec[idx + 1] * word[1]
            if(self.debug):
                print("num objects: ", number_of_objects)
            idx += 2
            # get "code" for correctly parsing values
            xyzQ_format_sum = byte_vec[idx] * word[0] + byte_vec[idx + 1] * word[1]
            #print("format: ", xyzQ_format_sum)
            xyzQ_format = 2 ** xyzQ_format_sum
            idx += 2
            inv_xyzQ_format = 1 / xyzQ_format

            x_arr = []
            y_arr = []
            doppler_arr = []


                # loop through all the objects
            for x in range(number_of_objects):
                x = byte_vec[idx] + byte_vec[idx+1] * 256
                y = byte_vec[idx+2] + byte_vec[idx+3] * 256

                if(x > 32767):
                    x = x - 65536

                if(y > 32767):
                    y = y - 65536

                x = x*inv_xyzQ_format
                y = y*inv_xyzQ_format

                vx = byte_vec[idx+4] + byte_vec[idx+5] * 256
                vy = byte_vec[idx+6] + byte_vec[idx+7] * 256

                if(vx > 32767):
                    vx = vx - 65536

                if(vy > 32767):
                    vy = vy - 65536

                vx = vx*inv_xyzQ_format
                vy = vy*inv_xyzQ_format

                depth = np.sqrt(y**2 + x**2)
                doppler = (vy*y+vx+x) / depth

                x_arr.append(x)
                y_arr.append(y)
                doppler_arr.append(doppler)
                # SPEED? CHECK WITH MAX MONDAY
                idx += self.TRACKER_STRUCT_SIZE_BYTES

                #print("X: ", x_arr)
                #print("Y: ", y_arr)
            print("SPEEDS: ", doppler_arr)
        return x_arr, y_arr, idx

    def parse_parking_assist(self,byte_vec, idx, tlv_length):
        idx = idx + tlv_length
        return idx

    def parse_stats(self,byte_vec, idx, tlv_length):
        idx = idx + tlv_length
        return idx

    def run_sampling(self, to_children,to_master):
        while True:
            # wait until we find the start code
            while self.barker_count != 8:
                res = self.s.read(1)
                res = res[0]
                if(res == self.header[self.barker_count]):
                    self.barker_count = self.barker_count + 1
                    if(self.debug):
                        self.array_confirmation.append(res)

            if(self.debug):
                print("ARRAY: ", self.array_confirmation)
            # if start code is found, read next 8 bytes. includes version and packetLength
            res = self.s.read(8)
            if(self.debug):
                print("RES: ", res)
            # calculate packetLength from reading
            packetLength = res[4] + res[5] * 256
            # 16 bytes of message have already been read in
            if(self.debug):
                print("PACKET LENGTH", packetLength)
            byte_vec = self.s.read(packetLength - 16)
            test_array = []
            for val in byte_vec:
                test_array.append(val)
            if(self.debug):
                print("BYTE VEC: ", test_array)
            self.idx = 0
            # get number of detected objects
            number_of_objects = byte_vec[self.idx + 12] + byte_vec[self.idx + 13] * 256

            # get number of TLVs
            number_of_tlvs = byte_vec[self.idx + 16] + byte_vec[self.idx + 17] * 256

            self.idx = 24

            if(self.debug):
                print("number of objects: " + str(number_of_objects))
                print("number_of_tlvs: " + str(number_of_tlvs))

            detection_x = []
            detection_y = []
            detection_type = []
            while self.idx < packetLength - 16:
                if(self.debug):
                    print("idx: ", self.idx)
                new_points, new_trackers, self.idx = self.tlv_switch(self.idx, byte_vec)

                # TYPE WILL KEEP TRACK OF WHETHER FOUND VALUES ARE POINTS OR TRACKELTS
                # O FOR POINTS, 1 FOR TRACKLETS
                # if len(new_points[0]) > 0:
                #     for x in new_points[0]:
                #         detection_x.append(x)
                #     for y in new_points[1]:
                #         detection_y.append(y)
                #         # only add 1 type per pair
                #         detection_type.append(0)
                if len(new_trackers[0]) > 0:
                    for x in new_trackers[0]:
                        detection_x.append(x)
                    for y in new_trackers[1]:
                        detection_y.append(y)
                        # only add 1 type per pair
                        detection_type.append(1)

            self.barker_count = 0
            self.array_confirmation = []
            self.idx = 0
            xy = {
                    'x': detection_x,
                    'y': detection_y,
                    't': detection_type,
                    's': 1
            }
            if to_master:
                to_master.send(xy)
            self.radar_storage.append((time.time(), "radar", xy))
            if to_children.poll(0):
                if to_children.recv() == "SAVE":
                    print("SAVING RADAR")
                    break

    def radar_playback(self, to_master):
        self.p = self.create_pipeline(data_path="data", start_time=0, end_time=None)
        while True:
            #to_master.send("FROM RADAR")
            packets = self.p.get_available_radar_packets()
            for radar_packet in packets:
                detections = list(radar_packet.getDetectedObjects())
                #to_master.send(detections)
                for obj in detections:
                    to_master.send(dict(obj))

    def record_radar(self, to_master,  to_children, path ):
        import time
        import numpy as np
        from pathlib import Path
        from concurrent.futures.thread import ThreadPoolExecutor
        import json
        import csv

        dest = Path(path).resolve().absolute()
        if dest.exists() and len(list(dest.glob('*'))) != 0:
            raise RuntimeError(
                f"Path {dest} contains {len(list(dest.glob('*')))} files. Either specify new path or remove files from this directory")
        dest.mkdir(parents=True, exist_ok=True)

        self.run_sampling(to_children, to_master)
        print("FINISHED RUNNING RADAR")
        with open(dest / Path('dataset.tsv'), 'w') as out_file:
            for ts, source, data in sorted([*self.radar_storage], key=lambda item: item[0]):
                save_array = []
                save_array.append(data)
                filename = Path(f"{int((ts - self.start_ts) * 10000)}-radar")
                filename = filename.with_suffix('.json')
                with open(dest / filename, 'w') as f:
                    json.dump(save_array, f)
                tsv_writer = csv.writer(out_file, delimiter='\t')
                tsv_writer.writerow([ts - self.start_ts, source, filename])
            if to_master:
                to_master.send("Finished saving frames Radar")



if __name__ == '__main__':
    RADAR = RadarSampler()
    RADAR.record_radar('data1')