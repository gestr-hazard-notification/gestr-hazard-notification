import argparse

import cv2

from cv_pipeline import MockupCNNPipeline

parser = argparse.ArgumentParser()

parser.add_argument('-p', '--path', default="data", type=str, help="Path where to store the captured data")
parser.add_argument('-s', '--start', default=0, type=int, help="Path where to store the captured data")
parser.add_argument('-e', '--end', default=None, type=int, help="Path where to store the captured data")

args = parser.parse_args()

create_pipeline = MockupCNNPipeline
p = create_pipeline(data_path=args.path, start_time=args.start, end_time=args.end)
detections = []


labels = [
            "background",
            "aeroplane",
            "bicycle",
            "bird",
            "boat",
            "bottle",
            "bus",
            "car",
            "cat",
            "chair",
            "cow",
            "diningtable",
            "dog",
            "horse",
            "motorbike",
            "person",
            "pottedplant",
            "sheep",
            "sofa",
            "train",
            "tvmonitor"
        ]

color_labels = [
            None,               #"background",
            None,               #"aeroplane",
            (27, 231, 255),     #"bicycle",
            None,               #"bird",
            None,               #"boat",
            None,               #"bottle",
            (110, 235, 131),    #"bus",
            (228, 255, 26),     #"car",
            None,               #"cat",
            None,               #"chair",
            None,               #"cow",
            None,               #"diningtable",
            (232, 170, 20),     #"dog",
            None,               #"horse",
            (255, 87, 20),      #"motorbike",
            (235, 110, 214),    #"person",
            None,               #"pottedplant",
            None,               #"sheep",
            None,               #"sofa",
            None,               #"train",
            None,               #"tvmonitor"
        ]
def show_bounding_boxes(frame, detections):

    frame_h = frame.shape[0]
    frame_w = frame.shape[1]
    for detection in detections:
        if color_labels[detection.label] != None:
            pt1 = int(detection.x_min * frame_w), int(detection.y_min * frame_h)
            pt2 = int(detection.x_max * frame_w), int(detection.y_max * frame_h)
            cv2.rectangle(frame, pt1, pt2, color_labels[detection.label], 2)

            x1, y1 = pt1
            x2, y2 = pt2

            cv2.rectangle(frame, pt1, pt2, color_labels[detection.label])
            # Handles case where TensorEntry object label is out if range
            if labels is not None:
                label = labels[detection.label]
            else:
                label = str(detection.label)

            pt_t1 = x1, y1 + 20
            cv2.putText(frame, label, pt_t1, cv2.FONT_HERSHEY_SIMPLEX, 0.5, color_labels[detection.label], 2)

            pt_t2 = x1, y1 + 40
            cv2.putText(frame, '{:.2f}'.format(100*detection.confidence) + ' %', pt_t2, cv2.FONT_HERSHEY_SIMPLEX, 0.5, color_labels[detection.label])

            pt_t3 = x1, y1 + 60
            cv2.putText(frame, 'x:' '{:7.3f}'.format(detection.depth_x) + ' m', pt_t3, cv2.FONT_HERSHEY_SIMPLEX, 0.5, color_labels[detection.label])

            pt_t4 = x1, y1 + 80
            cv2.putText(frame, 'y:' '{:7.3f}'.format(detection.depth_y) + ' m', pt_t4, cv2.FONT_HERSHEY_SIMPLEX, 0.5, color_labels[detection.label])

            pt_t5 = x1, y1 + 100
            cv2.putText(frame, 'z:' '{:7.3f}'.format(detection.depth_z) + ' m', pt_t5, cv2.FONT_HERSHEY_SIMPLEX, 0.5, color_labels[detection.label])

    return frame

while True:
    nnet_packets, data_packets = p.get_available_nnet_and_data_packets()

    for nnet_packet in nnet_packets:
        detections = list(nnet_packet.getDetectedObjects())

    for packet in data_packets:
        if packet.stream_name == 'previewout':
            data = packet.getData()
            data0 = data[0, :, :]
            data1 = data[1, :, :]
            data2 = data[2, :, :]
            frame = cv2.merge([data0, data1, data2])

            img_h = frame.shape[0]
            img_w = frame.shape[1]

            frame = show_bounding_boxes(frame, detections)

            cv2.imshow('previewout', frame)
        elif packet.stream_name in ('left', 'right'):
            frame = packet.getData()
            cv2.imshow(packet.stream_name, frame)

    if cv2.waitKey(1) == ord('q'):
        break
