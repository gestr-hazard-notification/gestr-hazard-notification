import multiprocessing, time
import sys

from RadarSampler import RadarSampler
from CVSampler import CVSampler
import matplotlib.pyplot as plt

def data_receiver(from_data_children):
    """
    function to print the messages received from other
    end of pipe
    """
    while True:
        msg = from_data_children.recv()
        #to_gui_receive.send(msg)
        print("Received the message: {}".format(msg))


def get_radar(conn):
    # RADAR = RadarSampler()
    # RADAR.radar_playback(conn)
    return 0

def get_cv(conn):
    CV = CVSampler()
    CV.cv_playback(conn)
    return 0

def start_pipe():
    data_parent_conn, data_child_conn = multiprocessing.Pipe()

    #gui_sender_conn, gui_receiver_conn = multiprocessing.Pipe()

    # creating new processes
    p1 = multiprocessing.Process(target=get_radar, args=(data_parent_conn,))
    # p2 = multiprocessing.Process(target=get_cv, args=(data_parent_conn,))
    p3 = multiprocessing.Process(target=data_receiver, args=(data_child_conn,))
    #p4 = multiprocessing.Process(target=gui_receiver, args=(gui_receiver_conn,))

        # running processes
    p1.start()
    # p2.start()
    p3.start()
    #p4.start()

    # wait until processes finish
    p1.join()
    # p2.join()
    p3.join()

if __name__ ==  '__main__':
    start_pipe()


