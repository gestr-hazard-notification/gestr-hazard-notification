import csv
import json
import time
from concurrent.futures.thread import ThreadPoolExecutor
from pathlib import Path

import numpy as np


class MockupMetadata:
    def __init__(self, ts):
        self.ts = ts

    def getTimestamp(self):
        return self.ts


class MockupDetection(dict):
    __getattr__ = dict.get


class MockupRadarPacket:
    def __init__(self, data, ts):
        if isinstance(data, list):
            self.raw = False
            self.data = [MockupDetection(item) for item in data]
        elif isinstance(data, dict):
            self.raw = True
            self.data = data

        self.metadata = MockupMetadata(ts)

    def getDetectedObjects(self):
        if self.raw:
            raise RuntimeError('raw is set. dont know what this means but figure me out if I print this')
        return self.data

    def get_tensor(self, name):
        if isinstance(name, int):
            return self.data[list(self.data.keys())[name]]
        else:
            return self.data[name]

    def getOutputsList(self):
        return list(self.data.values())

    def getOutputsDict(self):
        return self.data

    def __getitem__(self, name):
        return self.get_tensor(name)

    def getMetadata(self):
        return self.metadata


class MockupRadarPipeline:
    def __init__(self, data_path, start_time, end_time):
        self.dataset = {}
        with open(Path('/Users/jonathonmcnabb/school/gestr/gestr-hazard-notification/recording_and_playback/15mph_car_test_4-17r/dataset.tsv')) as fd:
            rd = csv.reader(fd, delimiter="\t", quotechar='"')
            for row in rd:
                self.dataset[row[2]] = {"ts": float(row[0]), "source": row[1], "data": row[2]}
        def _load_matched(path):
            filename = Path(path).name
            entry = self.dataset.get(filename, None)
            if entry is not None:
                with open(path) as fp:
                    return MockupRadarPacket(data=json.load(fp), ts=entry['ts'])


        with ThreadPoolExecutor(max_workers=100) as pool:
            self.radar_packets = list(sorted(pool.map(_load_matched, Path('/Users/jonathonmcnabb/school/gestr/gestr-hazard-notification/recording_and_playback/15mph_car_test_4-17r').glob('*.json')), key=lambda item: item.getMetadata().getTimestamp()))

        self.start_time = start_time + 1.3
        self.end_time = end_time

        self.frame_rate = 30

        self.started = None
        self.current_index_radar = 0

    def _get_available_packets(self, array, index):
        if self.started is None:
            self.started = time.time()
            return [], 0

        if self.end_time != None:
            stop_len = self.end_time*self.frame_rate
        else:
            stop_len = len(array)
        if index + 1 == stop_len:
            raise StopIteration()

        to_return = []
        for i, item in enumerate(array[index:]):
            if item.getMetadata().getTimestamp() < time.time() - self.started + self.start_time:
                to_return.append(item)
            else:
                index = index + i
                return to_return, index
        return to_return, index


    def get_available_radar_packets(self):
        new_packets, self.current_index_radar = self._get_available_packets(self.radar_packets, self.current_index_radar)
        return new_packets


if __name__ == "__main__":
    r_pipe = MockupRadarPipeline("data",0, None)
    while True:
        packets = r_pipe.get_available_radar_packets()
        for radar_packet in packets:
                detections = list(radar_packet.getDetectedObjects())
                for obj in detections:
                    print(obj)