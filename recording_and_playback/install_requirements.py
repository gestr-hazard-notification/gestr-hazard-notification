import os, sys, subprocess

pip_call = [sys.executable, "-m", "pip"]
pip_install = pip_call + ["install"]
subprocess.call([*pip_install, "-r", "requirements.txt"])