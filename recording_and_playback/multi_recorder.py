import multiprocessing
import time
import sys

from RadarSampler import RadarSampler
from CVSampler import CVSampler

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QApplication, QMainWindow

"""
Class to to manage multiprocessing operations
"""
class Multi:
	def __init__(self):
		# creating new processes
		self.to_master, self.from_detections = multiprocessing.Pipe()
		self.to_children, self.from_master = multiprocessing.Pipe()
	"""
	function to print the messages received from other
	end of pipe
	"""
	def data_manager(self, from_detections):
		while True:
			msg = from_detections.recv()
			print("Received the message: {}".format(msg))
	"""
	function to sample and record radar packets
	"""
	def record_radar(self,to_master, to_children):
		RADAR = RadarSampler()
		path= 'car_moving_in_sr'
		RADAR.record_radar(to_master, to_children, path)
		return 0
	"""
	function to sample and record CV packets
	"""
	def record_cv(self,to_master, to_children):
		CV = CVSampler()
		path = 'car_moving_in_scv'
		CV.record_depthai_mockups(to_master, to_children, path)
		return 0

	"""
	function to setup and start mutliprocessing
	"""
	def start_pipe(self):
		p1 = multiprocessing.Process(target=self.record_radar, args=(self.to_master,self.to_children,))
		p2 = multiprocessing.Process(target=self.record_cv, args=(self.to_master,self.to_children,))
		p3 = multiprocessing.Process(target=self.data_manager, args=(self.from_detections,))
		# p4 = multiprocessing.Process(target=gui_receiver, args=(gui_receiver_conn,))

		# running processes
		p1.start()
		p2.start()
		p3.start()
		# p4.start()
		while True:
			QtCore.QCoreApplication.processEvents()

		# wait until processes finish
		p1.join()
		p2.join()
		p3.join()

	"""
	function to send command to multiprocessing
	to begin saving buffered packets
	"""
	def stop_pipe(self):
		self.from_master.send("SAVE")
		time.sleep(0.1)
		self.from_master.send("SAVE")

class StarterWindow(QMainWindow):
	def __init__(self):
		super(StarterWindow,self).__init__()
		self.initUI()
		self.multi = Multi()

	def stop_clicked(self):
		self.multi.stop_pipe()
	def start_clicked(self):
		self.multi.start_pipe()


	def initUI(self):
		self.setGeometry(200, 200, 200, 200)
		self.setWindowTitle("Sampling Controller")
		self.b1 = QtWidgets.QPushButton(self)
		self.b1.setText("Start")
		self.b1.move(50,60)

		self.b1.clicked.connect(self.start_clicked)

		self.b2 = QtWidgets.QPushButton(self)
		self.b2.setText("Stop")
		self.b2.move(50,100)
		self.b2.clicked.connect(self.stop_clicked)

def start_app():
	app = QApplication(sys.argv)
	win = StarterWindow()
	win.show()
	sys.exit(app.exec_())

if __name__ ==  '__main__':
	start_app()


