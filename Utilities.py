# Lucas King
# Created 12/6/2020
# This is so we can convert from polar to cartesian and back, it also has other
# conversions that allow us to understand what is happening with the information we are getting
import math
import numpy as np

class Utility:
    #we can add any other variables we will need
    def _init_(self, distance, angle):
        float self.distance = distance
        float self.angle = angle
        self.x_point
        self.y_point

    def to_cartesian(self, distance, angle);
        #Angle must be in degrees otherwise we can convert it which is another function
        x_point = distance * math.cos(angle)
        y_point = distance * math.sin(angle)
        
        #I don't have a return statement here because I defined them as class varibles above.  However we can fix this
        # by creating an x and y function that prints out each one individually but then they wouldn't be stored
        
    def to_polar(self, x_point, y_point):
        # angle will be in degrees
        distance = math.sprt((x_point * x_point) + (y_point * y_point))
        angle = math.atan(y_point / x_point)
        
        #I don't have a return statement here because I defined them as class varibles above.  However we can fix this
        # by creating an x and y function that prints out each one individually but then they wouldn't be stored
    
    
if _name == "_main_":
    float d = 152
    float angle = 27
    car_one = Utility(d, angle)
    car_one.to_cartesian(d, angle)
    y_pos = car_one.y_point
    x_pos = car_one.x_point
    print("XY point is (" + x_pos + ", " + y_pos + ")")