# Gestr Hazard Notification Senior Design
The Github repository for Gestr:

* Developers:
    * Jonathon McNabb
    * Dan Parr
    * Lucas King
    * Nayef Haidar

# Organization
Code is organized by the following folders.

* `general_hazard/`
    * `detection/`
        * hazard detection algorithms
    * `object_classes/`
        * abstract class structure for different types of objects

* `Luxonis/`
    * `Demo/`
        * contains starter/demo code for luxonis devices
    * `DepthAI/`
        * `collision-avoidance/`
            * car collision prediction
        * `collecting training data/`
            * data collection

* `Testing/`
    * `testing_apps/`
        * `tracker/`
            * `starter_code/`
                * flutter starter project. Good reference
            * `tracker_app/`
                * hazard visualization phone app demo for bluetooth testing
