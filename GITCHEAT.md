# Git Cheat Sheet

## Commands
### Getting Started

#### Config: Run these commands before setting up repo

```
git config --global credential.helper store
git config --global user.name "John Doe"
git config --global user.email foo@mymail.mines.edu
```

#### Setup Repo
`git init`

or

`git clone url`

#### Common Commands:

**Standard Flow**
```bash
# See all commits
git log

# See status of your current git branch.
# Often will have advice on command that you need to run
git status

# Add modified file to be commited(aka stage the file)
git add filename

# Add all modified files to be commited(aka stage all files)
git add .

# Tell git not to track file anymore
git rm filename

# A short hand for commiting files and writing a commit message via one command
# must git add before using
git commit -m 'Some commit message'

# Add files and commit in one line.
# No need for git add
git commit -am 'Some commit message'

```

**Advanced: Ask if unsure!!**
```bash
# Unstage pending changes, the changes will still remain on file system
git reset

# Unstage pending changes, and reset files to pre-commit state. If
git reset --hard HEAD

# Go back to some time in history, on the current branch
git reset tag
git reset <commit-hash>

# Save current changes, without having to commit them to repo
git stash

# And later return those changes
git stash pop

# Return file to it's previous version, if it hasn’t been staged yet.
# Otherwise use git reset filename or git reset --hard filename
git checkout filename
```

**Comparing changes**
``` bash
# See current changes, that have not been staged yet.
# Good thing to check before running git add
git diff

# See current changes, that have not been commited yet (including staged changes)
git diff HEAD
```

### Working with Remote Branch
```bash
# See list of remote repos available. If you did git clone,
# you'll have at least one named "origin"
git remote

# Add a new remote. I.e. origin if it is not set
git remote add origin <https://some-git-remote-url>

# Push current branch to remote branch (usually with the same name)
# called upstream branch
git push

# If a remote branch is not set up as an upstream, you can make it so
# The -u tells Git to remember the parameters
git push -u origin master

# Otherwise you can manually specify remote and branch to use every time
git push origin branchname

# Just like pushing, you can get the latest updates from remote.
# By defaul Git will try to pull from "origin" and upstream branch
git pull

# Or you can tell git to pull a specific branch
git pull origin branchname

# Git pull, is actually a short hand for two command.
# Telling git to first fetch changes from a remote branch
# And then to merge them into current branch
git fetch && git merge origin/remote-branch-name
```
