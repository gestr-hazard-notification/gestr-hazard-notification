import time
import warnings

import detection.testing.trajectory_estimation_tester as trajectory_estimation_tester
import object_classes.testing.bus_tester as bus_tester
import object_classes.testing.car_tester as car_tester
import object_classes.testing.motor_cycle_tester as motor_cycle_tester
import object_classes.testing.object_manager_tester as object_manager_tester
import object_classes.testing.point_buffer_tester as point_buffer_tester

if __name__ == '__main__':

    # ignore numpy warnings. Comment to show warnings. 
    warnings.filterwarnings('ignore')

    start_time = time.time()

    car_tester.run_test()
    bus_tester.run_test()
    motor_cycle_tester.run_test()
    point_buffer_tester.run_test()
    object_manager_tester.run_test()
    # runs 90 simulations
    trajectory_estimation_tester.run_test()

    print("--- %s seconds ---" % (time.time() - start_time))

    print("\nEverything passed\n")
