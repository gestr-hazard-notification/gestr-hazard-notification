from object_classes.car import Car

item1 = {
    'timestamp': 0,
    'value': (0.0, 0.0)
}

item2 = {
    'timestamp': 0,
    'value': (1.0, 1.0)
}

item3 = {
    'timestamp': 0,
    'value': (2.0, 2.0)
}

c1 = Car(1, "AAAAAA", item1)
c2 = Car(2, "BBBBBB", item2)
c3 = Car(3, "CCCCCC", item3)

def test_license_plates():
    assert c1.license_plate == "AAAAAA", "c1 license plate should be: AAAAAA"
    assert c2.license_plate == "BBBBBB", "c2 license plate should be: BBBBBB"
    assert c3.license_plate == "CCCCCC", "c3 license plate should be: CCCCCC"

    c1.license_plate = "CCCCCC"
    c2.license_plate = "BBBBBB"
    c3.license_plate = "AAAAAA"

    assert c3.license_plate == "AAAAAA", "c3 license plate should be: AAAAAA"
    assert c2.license_plate == "BBBBBB", "c2 license plate should be: BBBBBB"
    assert c1.license_plate == "CCCCCC", "c1 license plate should be: CCCCCC"

def run_test():
    print("Running car tests...")
    test_license_plates()
