from object_classes.bus import Bus

item1 = {
    'timestamp': 0,
    'value': (0.0, 0.0)
}

item2 = {
    'timestamp': 0,
    'value': (1.0, 1.0)
}

item3 = {
    'timestamp': 0,
    'value': (2.0, 2.0)
}

b1 = Bus(1, "AAAAAA", item1)
b2 = Bus(2, "BBBBBB", item2)
b3 = Bus(3, "CCCCCC", item3)

def test_license_plates():
    assert b1.license_plate == "AAAAAA", "b1 license plate should be: AAAAAA"
    assert b2.license_plate == "BBBBBB", "b2 license plate should be: BBBBBB"
    assert b3.license_plate == "CCCCCC", "b3 license plate should be: CCCCCC"

    b1.license_plate = "CCCCCC"
    b2.license_plate = "BBBBBB"
    b3.license_plate = "AAAAAA"

    assert b3.license_plate == "AAAAAA", "b3 license plate should be: AAAAAA"
    assert b2.license_plate == "BBBBBB", "b2 license plate should be: BBBBBB"
    assert b1.license_plate == "CCCCCC", "b1 license plate should be: CCCCCC"

def run_test():
    print("Running bus tests...")
    test_license_plates()
