from object_classes.object_manager import ObjectManager

Om = ObjectManager()

item1 = {
    'timestamp': 0,
    'value': (0.0, 0.0)
}

item2 = {
    'timestamp': 0,
    'value': (1.0, 1.0)
}

item3 = {
    'timestamp': 0,
    'value': (2.0, 2.0)
}


def test_adding():
    assert Om.current_object_id == 0, "id should be 0"
    Om.add_car(item1)
    Om.add_car(item2)
    Om.add_car(item3)

    car = Om.get_object_by_id(2)
    assert car.object_id == 2, "car id should be 2"

    assert Om.current_object_id == 3, "id should be 3"

    Om.add_bus(item1)
    Om.add_bus(item2)
    Om.add_bus(item3)

    bus = Om.get_object_by_id(5)
    assert bus.object_id == 5, "bus id should be 5"

    assert Om.current_object_id == 6, "id should be 6"

    Om.add_motorcycle(item1)
    Om.add_motorcycle(item2)
    Om.add_motorcycle(item3)

    mc = Om.get_object_by_id(8)
    assert mc.object_id == 8, "mc id should be 8"

    assert Om.current_object_id == 9, "id should be 9"

    Om.add_person(item1)
    Om.add_person(item2)
    Om.add_person(item3)

    person = Om.get_object_by_id(11)
    assert person.object_id == 11, "person id should be 11"

    assert Om.current_object_id == 12, "id should be 12"

def run_test():
    print("Running object manager tests...")
    test_adding()

