from object_classes.car import Car

item01 = {
            'timestamp': 0, 
            'value': (0.0, 0.0)
        } 
item02 = {
            'timestamp': 0, 
            'value': (0.0, 0.0)
        } 

c1 = Car(1, "AAAAAA", item01, max_points=10)
c2 = Car(2, "BBBBBB", item02, max_points=15)

def test_point_buffer():
    currenty = 1.0
    currentz = 1.0
    
    for i in range(16):
        item = {
            'timestamp': i, 
            'value': (currenty, currentz)
        }  
        c1.add_coordinate(item)
        currenty += 1
        currentz += 1

    currenty = 1.0
    currentz = 1.0
    for i in range(16):
        item = {
            'timestamp': i, 
            'value': (currenty, currentz)
        }  
        c2.add_coordinate(item)
        currenty += 1
        currentz += 1

    c1_buffer_test = [{'timestamp': 6, 'value': (7.0, 7.0)}, {'timestamp': 7, 'value': (8.0, 8.0)}, {'timestamp': 8, 'value': (9.0, 9.0)}, {'timestamp': 9, 'value': (10.0, 10.0)}, {'timestamp': 10, 'value': (11.0, 11.0)}, {'timestamp': 11, 'value': (12.0, 12.0)}, {'timestamp': 12, 'value': (13.0, 13.0)}, {'timestamp': 13, 'value': (14.0, 14.0)}, {'timestamp': 14, 'value': (15.0, 15.0)}, {'timestamp': 15, 'value': (16.0, 16.0)}]
    c2_buffer_test = [{'timestamp': 1, 'value': (2.0, 2.0)}, {'timestamp': 2, 'value': (3.0, 3.0)}, {'timestamp': 3, 'value': (4.0, 4.0)}, {'timestamp': 4, 'value': (5.0, 5.0)}, {'timestamp': 5, 'value': (6.0, 6.0)}, {'timestamp': 6, 'value': (7.0, 7.0)}, {'timestamp': 7, 'value': (8.0, 8.0)}, {'timestamp': 8, 'value': (9.0, 9.0)}, {'timestamp': 9, 'value': (10.0, 10.0)}, {'timestamp': 10, 'value': (11.0, 11.0)}, {'timestamp': 11, 'value': (12.0, 12.0)}, {'timestamp': 12, 'value': (13.0, 13.0)}, {'timestamp': 13, 'value': (14.0, 14.0)}, {'timestamp': 14, 'value': (15.0, 15.0)}, {'timestamp': 15, 'value': (16.0, 16.0)}]

    assert c1.position_history == c1_buffer_test, "c1 buffer is incorrect"
    assert c2.position_history == c2_buffer_test, "c2 buffer is incorrect"
    

def run_test():
    print("Running point buffer tests...")
    test_point_buffer()
