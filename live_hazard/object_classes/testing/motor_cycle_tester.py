from object_classes.motor_cycle import MotorCycle

item1 = {
    'timestamp': 0,
    'value': (0.0, 0.0)
}

item2 = {
    'timestamp': 0,
    'value': (1.0, 1.0)
}

item3 = {
    'timestamp': 0,
    'value': (2.0, 2.0)
}

mc1 = MotorCycle(1, "AAAAAA", item1)
mc2 = MotorCycle(2, "BBBBBB", item2)
mc3 = MotorCycle(3, "CCCCCC", item3)

def test_license_plates():
    assert mc1.license_plate == "AAAAAA", "mc1 license plate should be: AAAAAA"
    assert mc2.license_plate == "BBBBBB", "mc2 license plate should be: BBBBBB"
    assert mc3.license_plate == "CCCCCC", "mc3 license plate should be: CCCCCC"

    mc1.license_plate = "CCCCCC"
    mc2.license_plate = "BBBBBB"
    mc3.license_plate = "AAAAAA"

    assert mc3.license_plate == "AAAAAA", "mc3 license plate should be: AAAAAA"
    assert mc2.license_plate == "BBBBBB", "mc2 license plate should be: BBBBBB"
    assert mc1.license_plate == "CCCCCC", "mc1 license plate should be: CCCCCC"

def run_test():
    print("Running motor cycle tests...")
    test_license_plates()
