from object_classes.detectable_object import DetectableObject


class Vehicle(DetectableObject):
    def __init__(self, object_id, license_plate, point=None, max_points=10):
        super().__init__(object_id, point, max_points)
        self.license_plate = license_plate
        

    @property  
    def license_plate(self):
        return self._license_plate

    @license_plate.setter
    def license_plate(self, license_plate):
        self._license_plate = license_plate




