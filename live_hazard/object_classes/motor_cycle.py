from object_classes.vehicle import Vehicle


class MotorCycle(Vehicle):
    def __init__(self, object_id, license_plate, point=None, num_points=10):
        super().__init__(object_id, license_plate, point, num_points)

    def hazard_detection(self):
        pass
