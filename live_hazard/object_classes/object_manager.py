from collections import OrderedDict

from object_classes.bus import Bus
from object_classes.car import Car
from object_classes.motor_cycle import MotorCycle
from object_classes.person import Person
from object_classes.general import General


class ObjectManager:
    current_object_id = 0
    def __init__(self):
        self.objects = OrderedDict()

    def __add_car(self, point=None, license_plate=None):
        new_car = Car(ObjectManager.current_object_id, license_plate, point)
        self.objects[ObjectManager.current_object_id] = new_car
        self.increment_object_id()

    def __add_bus(self, point=None, license_plate=None):
        new_bus = Bus(ObjectManager.current_object_id, license_plate, point)
        self.objects[ObjectManager.current_object_id] = new_bus
        self.increment_object_id()

    def __add_motorcycle(self, point=None, license_plate=None):
        new_cycle = MotorCycle(ObjectManager.current_object_id, license_plate, point)
        self.objects[ObjectManager.current_object_id] = new_cycle
        self.increment_object_id()

    def __add_person(self, point=None):
        new_person = Person(ObjectManager.current_object_id, point)
        self.objects[ObjectManager.current_object_id] = new_person
        self.increment_object_id()

    def __add_general(self, point=None):
        new_general = General(ObjectManager.current_object_id, point)
        self.obkects[ObjectManager.current_object_id] = new_general
        self.__increment_object_id

    def add_new_object(self, label=None, point=None, license_plate=None):
        if label == "car":
            self.__add_car(point=point, license_plate=license_plate)
        else if label == "bus":
            self.__add_bus(point=point, license_plate=license_plate)
        else if label == "motorbike":
            self.__add_motorcycle(point=point,license_plate=license_plate)
        else if lablel == "person":
            self.__add_person(point=point)
        else:
            self.__add_general(point=point)


    def get_object_by_id(self, object_id):
        return self.objects[object_id]

    def __increment_object_id(self):
        ObjectManager.current_object_id += 1

