from detectable_object import DetectableObject
import math
import time
import numpy as np


start_time = time.time()
q = 0
obj_arr = np.array([])
##function measures distance between cv object and radar object coordinates. If they are within a certain radius,
##they are combined and used to create a detected object
def combine(detect_cv,detect_radar,radius):
    global q, obj_arr
    index_c = np.array([])
    index_r = np.array([])
    for i in range(len(detect_cv)):
        #find where radar element matches cv element; then add that element to object array:
        for j in range(len(detect_radar)):
            R = math.sqrt((detect_cv[i][0]-detect_radar[j][0])**2+(detect_cv[i][1]-detect_radar[j][1])**2)
            if R <= radius: #cv and radar detect same object
                index_c = np.append(index_c,i)
                index_r = np.append(index_r,j)
                obj_coord = [(detect_cv[i][0]+detect_radar[j][0])/2,(detect_cv[i][1]+detect_radar[j][1])/2]
                obj_q = DetectableObject(q,obj_coord,1)
                obj_arr = np.append(obj_arr,obj_q) #create a detected object and add to list
                q += 1
            elif max(detect_radar[j]) >= 40 and j not in index_r: #create object from radar if it is outside range of CV
                index_r = np.append(index_r,j)
                obj_coord = [detect_radar[j][0],detect_radar[j][1]]
                obj_q = DetectableObject(q,obj_coord,1)
                obj_arr = np.append(obj_arr,obj_q) #create a detected object and add to list
                q += 1
    detect_cv = [m for n, m in enumerate(detect_cv) if n not in index_c] #remove the detected object from cv list
    detect_radar = [m for n, m in enumerate(detect_radar) if n not in index_r] #remove the detected object from radar list
    return obj_arr, detect_cv, detect_radar

##function compares cv and radar coordinates to coordinates of already detected objects. If there are matches, those cv and radar
##coordinates are rejected. If there are no matches, it compares cv and radar coordinates to find new detected objects.
def nearest(obj_arr,detect_cv,detect_radar,radius):
    print('cv_arr:', detect_cv)
    print('radar_arr:', detect_radar)
    index_c = np.array([])
    index_r = np.array([])
    if len(obj_arr) == 0: #if no objects detected yet
        #create detected objects from cv and radar coordinates if they match:
        obj_arr, detect_cv, detect_radar = combine(detect_cv,detect_radar,radius)
    else: #some objects have already been detected
        for i in range(len(obj_arr)):
            #find where cv element matches object element; then remove cv element from cv array:
            for j in range(len(detect_cv)):
                R = math.sqrt((obj_arr[i].position_history[0][0]-detect_cv[j][0])**2+(obj_arr[i].position_history[0][1]-detect_cv[j][1])**2)
                if R <= radius:
                    index_c = np.append(index_c,j)
            #find where radar element matches object element; then remove radar element from radar array:
            for k in range(len(detect_radar)):
                R = math.sqrt((obj_arr[i].position_history[0][0]-detect_radar[k][0])**2+(obj_arr[i].position_history[0][1]-detect_radar[k][1])**2)
                if R <= radius:
                    index_r = np.append(index_r,k)
        detect_cv = [m for n, m in enumerate(detect_cv) if n not in index_c] #remove the detected object from cv list
        detect_radar = [m for n, m in enumerate(detect_radar) if n not in index_r] #remove the detected object from radar list
        print('intermediate_cv_arr:', detect_cv)
        print('intermediate_radar_arr:', detect_radar)
        #create detected objects from remaining cv and radar coordinates if they match:
        obj_arr, detect_cv, detect_radar = combine(detect_cv,detect_radar,radius)
    print('new_cv_arr:', detect_cv)
    print('new_radar_arr:', detect_radar)
    return obj_arr, detect_cv, detect_radar




#The following code tests these functions:
det_objs = np.array([]) #no detected objects yet
cv_test1 = np.array([[9,11.5],[-3.5,19.5],[0.5,2]]) #cv sees this in first pass
radar_test1 = np.array([[-4,20.5],[10,10],[3,28],[0,45],[-4,80]]) #radar sees this in first pass
radius_filter = 2
det_objs, cv_coords, radar_coords = nearest(det_objs,cv_test1,radar_test1,radius_filter)
print()
print('Objects detected in first pass:')
for i in range(len(det_objs)):
    print(det_objs[i].position_history)
print()

cv_test2 = np.array([[9,11],[-3,19],[0.5,1.5],[2,29]]) #cv sees this in second pass
radar_test2 = np.array([[-3.5,19.5],[10,10],[0,45],[3,28],[-4,80]]) #radar sees this in second pass
det_objs, cv_coords, radar_coords = nearest(det_objs,cv_test2,radar_test2,radius_filter)
print()
print('Objects detected in second pass:')
for j in range(len(det_objs)):
    print(det_objs[j].position_history)
print()
print('Running time: ', time.time() - start_time)



##pseudocode algorithm:
#loop through existing
    #check radius of existing
    #group objects within r
    #remove objects from detect_cv and detect_radar
    #add grouped to obj_arr
#loop through remaining detect_cv and detect_radar
    #make combinations
    #add combinations
    #add singles from radar outside range of CV


