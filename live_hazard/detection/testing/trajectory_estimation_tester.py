from detection.trajectory_estimation import TrajectoryEstimation

import math

T1 = TrajectoryEstimation()

def test_trajectory_estimation():

    # SHOULD COLLIDE

    test_buffer0 = generate_points(0, 2, True, True)
    test_buffer1 = generate_points(5, 2, True, True)
    test_buffer2 = generate_points(10, 2, True, True)
    test_buffer3 = generate_points(15, 2, True, True)
    test_buffer4 = generate_points(20, 2, True, True)
    test_buffer5 = generate_points(25, 2, True, True)
    test_buffer6 = generate_points(30, 2, True, True)
    test_buffer7 = generate_points(35, 2, True, True)
    test_buffer8 = generate_points(40, 2, True, True)
    test_buffer9 = generate_points(45, 2, True, True)
    test_buffer10 = generate_points(50, 2, True, True)
    test_buffer11 = generate_points(55, 2, True, True)
    test_buffer12 = generate_points(60, 2, True, True)
    test_buffer13 = generate_points(65, 2, True, True)
    test_buffer14 = generate_points(70, 2, True, True)
    test_buffer15 = generate_points(75, 2, True, True)
    test_buffer16 = generate_points(80, 2, True, True)
    test_buffer17 = generate_points(85, 2, True, True)
    test_buffer18 = generate_points(90, 2, True, True)

    
    is_buffer0_dangerous = T1.driving_towards_bike(test_buffer0)
    is_buffer1_dangerous = T1.driving_towards_bike(test_buffer1)
    is_buffer2_dangerous = T1.driving_towards_bike(test_buffer2)
    is_buffer3_dangerous = T1.driving_towards_bike(test_buffer3)
    is_buffer4_dangerous = T1.driving_towards_bike(test_buffer4)
    is_buffer5_dangerous = T1.driving_towards_bike(test_buffer5)
    is_buffer6_dangerous = T1.driving_towards_bike(test_buffer6)
    is_buffer7_dangerous = T1.driving_towards_bike(test_buffer7)
    is_buffer8_dangerous = T1.driving_towards_bike(test_buffer8)
    is_buffer9_dangerous = T1.driving_towards_bike(test_buffer9)
    is_buffer10_dangerous = T1.driving_towards_bike(test_buffer10)
    is_buffer11_dangerous = T1.driving_towards_bike(test_buffer11)
    is_buffer12_dangerous = T1.driving_towards_bike(test_buffer12)
    is_buffer13_dangerous = T1.driving_towards_bike(test_buffer13)
    is_buffer14_dangerous = T1.driving_towards_bike(test_buffer14)
    is_buffer15_dangerous = T1.driving_towards_bike(test_buffer15)
    is_buffer16_dangerous = T1.driving_towards_bike(test_buffer16)
    is_buffer17_dangerous = T1.driving_towards_bike(test_buffer17)
    is_buffer18_dangerous = T1.driving_towards_bike(test_buffer18)

    # EDGE CASE WHEN SPEED = 0
    assert is_buffer0_dangerous == False, "buffer0_1 IS NOT a dangerous trajectory"

    # ALL OTHERS DANGEROUS
    assert is_buffer1_dangerous == True, "buffer1_1 IS a dangerous trajectory"
    assert is_buffer2_dangerous == True, "buffer2_1 IS a dangerous trajectory"
    assert is_buffer3_dangerous == True, "buffer3_1 IS a dangerous trajectory"
    assert is_buffer4_dangerous == True, "buffer4_1 IS a dangerous trajectory"
    assert is_buffer5_dangerous == True, "buffer5_1 IS a dangerous trajectory"
    assert is_buffer6_dangerous == True, "buffer6_1 IS a dangerous trajectory"
    assert is_buffer7_dangerous == True, "buffer7_1 IS a dangerous trajectory"
    assert is_buffer8_dangerous == True, "buffer8_1 IS a dangerous trajectory"
    assert is_buffer9_dangerous == True, "buffer9_1 IS a dangerous trajectory"
    assert is_buffer10_dangerous == True, "buffer10_1 IS a dangerous trajectory"
    assert is_buffer11_dangerous == True, "buffer11_1 IS a dangerous trajectory"
    assert is_buffer12_dangerous == True, "buffer12_1 IS a dangerous trajectory"
    assert is_buffer13_dangerous == True, "buffer13_1 IS a dangerous trajectory"
    assert is_buffer14_dangerous == True, "buffer14_1 IS a dangerous trajectory"
    assert is_buffer15_dangerous == True, "buffer15_1 IS a dangerous trajectory"
    assert is_buffer16_dangerous == True, "buffer16_1 IS a dangerous trajectory"
    assert is_buffer17_dangerous == True, "buffer17_1 IS a dangerous trajectory"
    assert is_buffer18_dangerous == True, "buffer18_1 IS a dangerous trajectory"

    # SHOULD NOT COLLIDE
    test_buffer0 = generate_points(0, 2, False, True)
    test_buffer1 = generate_points(5, 2, False, True)
    test_buffer2 = generate_points(10, 2, False, True)
    test_buffer3 = generate_points(15, 2, False, True)
    test_buffer4 = generate_points(20, 2, False, True)
    test_buffer5 = generate_points(25, 2, False, True)
    test_buffer6 = generate_points(30, 2, False, True)
    test_buffer7 = generate_points(35, 2, False, True)
    test_buffer8 = generate_points(40, 2, False, True)
    test_buffer9 = generate_points(45, 2, False, True)
    test_buffer10 = generate_points(50, 2, False, True)
    test_buffer11 = generate_points(55, 2, False, True)
    test_buffer12 = generate_points(60, 2, False, True)
    test_buffer13 = generate_points(65, 2, False, True)
    test_buffer14 = generate_points(70, 2, False, True)
    test_buffer15 = generate_points(75, 2, False, True)
    test_buffer16 = generate_points(80, 2, False, True)
    test_buffer17 = generate_points(85, 2, False, True)
    test_buffer18 = generate_points(90, 2, False, True)

    
    is_buffer0_dangerous = T1.driving_towards_bike(test_buffer0)
    is_buffer1_dangerous = T1.driving_towards_bike(test_buffer1)
    is_buffer2_dangerous = T1.driving_towards_bike(test_buffer2)
    is_buffer3_dangerous = T1.driving_towards_bike(test_buffer3)
    is_buffer4_dangerous = T1.driving_towards_bike(test_buffer4)
    is_buffer5_dangerous = T1.driving_towards_bike(test_buffer5)
    is_buffer6_dangerous = T1.driving_towards_bike(test_buffer6)
    is_buffer7_dangerous = T1.driving_towards_bike(test_buffer7)
    is_buffer8_dangerous = T1.driving_towards_bike(test_buffer8)
    is_buffer9_dangerous = T1.driving_towards_bike(test_buffer9)
    is_buffer10_dangerous = T1.driving_towards_bike(test_buffer10)
    is_buffer11_dangerous = T1.driving_towards_bike(test_buffer11)
    is_buffer12_dangerous = T1.driving_towards_bike(test_buffer12)
    is_buffer13_dangerous = T1.driving_towards_bike(test_buffer13)
    is_buffer14_dangerous = T1.driving_towards_bike(test_buffer14)
    is_buffer15_dangerous = T1.driving_towards_bike(test_buffer15)
    is_buffer16_dangerous = T1.driving_towards_bike(test_buffer16)
    is_buffer17_dangerous = T1.driving_towards_bike(test_buffer17)
    is_buffer18_dangerous = T1.driving_towards_bike(test_buffer18)

    assert is_buffer0_dangerous == False, "buffer0_2 IS NOT a dangerous trajectory"
    assert is_buffer1_dangerous == False, "buffer1_2 IS NOT a dangerous trajectory"
    assert is_buffer2_dangerous == False, "buffer2_2 IS NOT a dangerous trajectory"
    assert is_buffer3_dangerous == False, "buffer3_2 IS NOT a dangerous trajectory"
    assert is_buffer4_dangerous == False, "buffer4_2 IS NOT a dangerous trajectory"
    assert is_buffer5_dangerous == False, "buffer5_2 IS NOT a dangerous trajectory"
    assert is_buffer6_dangerous == False, "buffer6_2 IS NOT a dangerous trajectory"
    assert is_buffer7_dangerous == False, "buffer7_2 IS NOT a dangerous trajectory"
    assert is_buffer8_dangerous == False, "buffer8_2 IS NOT a dangerous trajectory"
    assert is_buffer9_dangerous == False, "buffer9_2 IS NOT a dangerous trajectory"
    assert is_buffer10_dangerous == False, "buffer10_2 IS NOT a dangerous trajectory"
    assert is_buffer11_dangerous == False, "buffer11_2 IS NOT a dangerous trajectory"
    assert is_buffer12_dangerous == False, "buffer12_2 IS NOT a dangerous trajectory"
    assert is_buffer13_dangerous == False, "buffer13_2 IS NOT a dangerous trajectory"
    assert is_buffer14_dangerous == False, "buffer14_2 IS NOT a dangerous trajectory"
    assert is_buffer15_dangerous == False, "buffer15_2 IS NOT a dangerous trajectory"
    assert is_buffer16_dangerous == False, "buffer16_2 IS NOT a dangerous trajectory"
    assert is_buffer17_dangerous == False, "buffer17_2 IS NOT a dangerous trajectory"
    assert is_buffer18_dangerous == False, "buffer18_2 IS NOT a dangerous trajectory"

    # SHOULD NOT COLLIDE
    test_buffer0 = generate_points(0, -2, True, False)
    test_buffer1 = generate_points(5, -2, True, False)
    test_buffer2 = generate_points(10, -2, True, False)
    test_buffer3 = generate_points(15, -2, True, False)
    test_buffer4 = generate_points(20, -2, True, False)
    test_buffer5 = generate_points(25, -2, True, False)
    test_buffer6 = generate_points(30, -2, True, False)
    test_buffer7 = generate_points(35, -2, True, False)
    test_buffer8 = generate_points(40, -2, True, False)
    test_buffer9 = generate_points(45, -2, True, False)
    test_buffer10 = generate_points(50, -2, True, False)
    test_buffer11 = generate_points(55, -2, True, False)
    test_buffer12 = generate_points(60, -2, True, False)
    test_buffer13 = generate_points(65, -2, True, False)
    test_buffer14 = generate_points(70, -2, True, False)
    test_buffer15 = generate_points(75, -2, True, False)
    test_buffer16 = generate_points(80, -2, True, False)
    test_buffer17 = generate_points(85, -2, True, False)
    test_buffer18 = generate_points(90, -2, True, False)

    
    is_buffer0_dangerous = T1.driving_towards_bike(test_buffer0)
    is_buffer1_dangerous = T1.driving_towards_bike(test_buffer1)
    is_buffer2_dangerous = T1.driving_towards_bike(test_buffer2)
    is_buffer3_dangerous = T1.driving_towards_bike(test_buffer3)
    is_buffer4_dangerous = T1.driving_towards_bike(test_buffer4)
    is_buffer5_dangerous = T1.driving_towards_bike(test_buffer5)
    is_buffer6_dangerous = T1.driving_towards_bike(test_buffer6)
    is_buffer7_dangerous = T1.driving_towards_bike(test_buffer7)
    is_buffer8_dangerous = T1.driving_towards_bike(test_buffer8)
    is_buffer9_dangerous = T1.driving_towards_bike(test_buffer9)
    is_buffer10_dangerous = T1.driving_towards_bike(test_buffer10)
    is_buffer11_dangerous = T1.driving_towards_bike(test_buffer11)
    is_buffer12_dangerous = T1.driving_towards_bike(test_buffer12)
    is_buffer13_dangerous = T1.driving_towards_bike(test_buffer13)
    is_buffer14_dangerous = T1.driving_towards_bike(test_buffer14)
    is_buffer15_dangerous = T1.driving_towards_bike(test_buffer15)
    is_buffer16_dangerous = T1.driving_towards_bike(test_buffer16)
    is_buffer17_dangerous = T1.driving_towards_bike(test_buffer17)
    is_buffer18_dangerous = T1.driving_towards_bike(test_buffer18)

    assert is_buffer0_dangerous == False, "buffer0_3 IS NOT a dangerous trajectory"
    assert is_buffer1_dangerous == False, "buffer1_3 IS NOT a dangerous trajectory"
    assert is_buffer2_dangerous == False, "buffer2_3 IS NOT a dangerous trajectory"
    assert is_buffer3_dangerous == False, "buffer3_3 IS NOT a dangerous trajectory"
    assert is_buffer4_dangerous == False, "buffer4_3 IS NOT a dangerous trajectory"
    assert is_buffer5_dangerous == False, "buffer5_3 IS NOT a dangerous trajectory"
    assert is_buffer6_dangerous == False, "buffer6_3 IS NOT a dangerous trajectory"
    assert is_buffer7_dangerous == False, "buffer7_3 IS NOT a dangerous trajectory"
    assert is_buffer8_dangerous == False, "buffer8_3 IS NOT a dangerous trajectory"
    assert is_buffer9_dangerous == False, "buffer9_3 IS NOT a dangerous trajectory"
    assert is_buffer10_dangerous == False, "buffer10_3 IS NOT a dangerous trajectory"
    assert is_buffer11_dangerous == False, "buffer11_3 IS NOT a dangerous trajectory"
    assert is_buffer12_dangerous == False, "buffer12_3 IS NOT a dangerous trajectory"
    assert is_buffer13_dangerous == False, "buffer13_3 IS NOT a dangerous trajectory"
    assert is_buffer14_dangerous == False, "buffer14_3 IS NOT a dangerous trajectory"
    assert is_buffer15_dangerous == False, "buffer15_3 IS NOT a dangerous trajectory"
    assert is_buffer16_dangerous == False, "buffer16_3 IS NOT a dangerous trajectory"
    assert is_buffer17_dangerous == False, "buffer17_3 IS NOT a dangerous trajectory"
    assert is_buffer18_dangerous == False, "buffer18_3 IS NOT a dangerous trajectory"

    # SHOULD COLLIDE

    test_buffer0 = generate_points(0, -1, True, True)
    test_buffer1 = generate_points(5, -1, True, True)
    test_buffer2 = generate_points(10, -1, True, True)
    test_buffer3 = generate_points(15, -1, True, True)
    test_buffer4 = generate_points(20, -1, True, True)
    test_buffer5 = generate_points(25, -1, True, True)
    test_buffer6 = generate_points(30, -1, True, True)
    test_buffer7 = generate_points(35, -1, True, True)
    test_buffer8 = generate_points(40, -1, True, True)
    test_buffer9 = generate_points(45, -1, True, True)
    test_buffer10 = generate_points(50, -1, True, True)
    test_buffer11 = generate_points(55, -1, True, True)
    test_buffer12 = generate_points(60, -1, True, True)
    test_buffer13 = generate_points(65, -1, True, True)
    test_buffer14 = generate_points(70, -1, True, True)
    test_buffer15 = generate_points(75, -1, True, True)
    test_buffer16 = generate_points(80, -1, True, True)
    test_buffer17 = generate_points(85, -1, True, True)
    test_buffer18 = generate_points(90, -1, True, True)

    
    is_buffer0_dangerous = T1.driving_towards_bike(test_buffer0)
    is_buffer1_dangerous = T1.driving_towards_bike(test_buffer1)
    is_buffer2_dangerous = T1.driving_towards_bike(test_buffer2)
    is_buffer3_dangerous = T1.driving_towards_bike(test_buffer3)
    is_buffer4_dangerous = T1.driving_towards_bike(test_buffer4)
    is_buffer5_dangerous = T1.driving_towards_bike(test_buffer5)
    is_buffer6_dangerous = T1.driving_towards_bike(test_buffer6)
    is_buffer7_dangerous = T1.driving_towards_bike(test_buffer7)
    is_buffer8_dangerous = T1.driving_towards_bike(test_buffer8)
    is_buffer9_dangerous = T1.driving_towards_bike(test_buffer9)
    is_buffer10_dangerous = T1.driving_towards_bike(test_buffer10)
    is_buffer11_dangerous = T1.driving_towards_bike(test_buffer11)
    is_buffer12_dangerous = T1.driving_towards_bike(test_buffer12)
    is_buffer13_dangerous = T1.driving_towards_bike(test_buffer13)
    is_buffer14_dangerous = T1.driving_towards_bike(test_buffer14)
    is_buffer15_dangerous = T1.driving_towards_bike(test_buffer15)
    is_buffer16_dangerous = T1.driving_towards_bike(test_buffer16)
    is_buffer17_dangerous = T1.driving_towards_bike(test_buffer17)
    is_buffer18_dangerous = T1.driving_towards_bike(test_buffer18)

    assert is_buffer0_dangerous == False, "buffer0_4 IS NOT a dangerous trajectory"
    assert is_buffer1_dangerous == True, "buffer1_4 IS a dangerous trajectory"
    assert is_buffer2_dangerous == True, "buffer2_4 IS a dangerous trajectory"
    assert is_buffer3_dangerous == True, "buffer3_4 IS a dangerous trajectory"
    assert is_buffer4_dangerous == True, "buffer4_4 IS a dangerous trajectory"
    assert is_buffer5_dangerous == True, "buffer5_4 IS a dangerous trajectory"
    assert is_buffer6_dangerous == True, "buffer6_4 IS a dangerous trajectory"
    assert is_buffer7_dangerous == True, "buffer7_4 IS a dangerous trajectory"
    assert is_buffer8_dangerous == True, "buffer8_4 IS a dangerous trajectory"
    assert is_buffer9_dangerous == True, "buffer9_4 IS a dangerous trajectory"
    assert is_buffer10_dangerous == True, "buffer10_4 IS a dangerous trajectory"
    assert is_buffer11_dangerous == True, "buffer11_4 IS a dangerous trajectory"
    assert is_buffer12_dangerous == True, "buffer12_4 IS a dangerous trajectory"
    assert is_buffer13_dangerous == True, "buffer13_4 IS a dangerous trajectory"
    assert is_buffer14_dangerous == True, "buffer14_4 IS a dangerous trajectory"
    assert is_buffer15_dangerous == True, "buffer15_4 IS a dangerous trajectory"
    assert is_buffer16_dangerous == True, "buffer16_4 IS a dangerous trajectory"
    assert is_buffer17_dangerous == True, "buffer17_4 IS a dangerous trajectory"
    assert is_buffer18_dangerous == True, "buffer18_4 IS a dangerous trajectory"

def test_impact():
    # SHOULD COLLIDE

    test_buffer0 = generate_points(0, 2, True, True)
    test_buffer1 = generate_points(5, 2, True, True)
    test_buffer2 = generate_points(10, 2, True, True)
    test_buffer3 = generate_points(15, 2, True, True)
    test_buffer4 = generate_points(20, 2, True, True)
    test_buffer5 = generate_points(25, 2, True, True)
    test_buffer6 = generate_points(30, 2, True, True)
    test_buffer7 = generate_points(35, 2, True, True)
    test_buffer8 = generate_points(40, 2, True, True)
    test_buffer9 = generate_points(45, 2, True, True)
    test_buffer10 = generate_points(50, 2, True, True)
    test_buffer11 = generate_points(55, 2, True, True)
    test_buffer12 = generate_points(60, 2, True, True)
    test_buffer13 = generate_points(65, 2, True, True)
    test_buffer14 = generate_points(70, 2, True, True)
    test_buffer15 = generate_points(75, 2, True, True)
    test_buffer16 = generate_points(80, 2, True, True)
    test_buffer17 = generate_points(85, 2, True, True)
    test_buffer18 = generate_points(90, 2, True, True)

    
    is_buffer0_impact = T1.is_impact_close(test_buffer0)
    is_buffer1_impact = T1.is_impact_close(test_buffer1)
    is_buffer2_impact = T1.is_impact_close(test_buffer2)
    is_buffer3_impact = T1.is_impact_close(test_buffer3)
    is_buffer4_impact = T1.is_impact_close(test_buffer4)
    is_buffer5_impact = T1.is_impact_close(test_buffer5)
    is_buffer6_impact = T1.is_impact_close(test_buffer6)
    is_buffer7_impact = T1.is_impact_close(test_buffer7)
    is_buffer8_impact = T1.is_impact_close(test_buffer8)
    is_buffer9_impact = T1.is_impact_close(test_buffer9)
    is_buffer10_impact = T1.is_impact_close(test_buffer10)
    is_buffer11_impact = T1.is_impact_close(test_buffer11)
    is_buffer12_impact = T1.is_impact_close(test_buffer12)
    is_buffer13_impact = T1.is_impact_close(test_buffer13)
    is_buffer14_impact = T1.is_impact_close(test_buffer14)
    is_buffer15_impact = T1.is_impact_close(test_buffer15)
    is_buffer16_impact = T1.is_impact_close(test_buffer16)
    is_buffer17_impact = T1.is_impact_close(test_buffer17)
    is_buffer18_impact = T1.is_impact_close(test_buffer18)

    assert is_buffer0_impact == False, "buffer0_5 IS NOT a impact trajectory"
    assert is_buffer1_impact == False, "buffer1_5 IS NOT a impact trajectory"
    assert is_buffer2_impact == False, "buffer2_5 IS NOT a impact trajectory"
    assert is_buffer3_impact == False, "buffer3_5 IS NOT a impact trajectory"
    assert is_buffer4_impact == False, "buffer4_5 IS NOT a impact trajectory"
    assert is_buffer5_impact == False, "buffer5_5 IS NOT a impact trajectory"
    assert is_buffer6_impact == False, "buffer6_5 IS NOT a impact trajectory"
    assert is_buffer7_impact == False, "buffer7_5 IS NOT a impact trajectory"
    assert is_buffer8_impact == True, "buffer8_5 IS a impact trajectory"
    assert is_buffer9_impact == True, "buffer9_5 IS a impact trajectory"
    assert is_buffer10_impact == True, "buffer10_5 IS a impact trajectory"
    assert is_buffer11_impact == True, "buffer11_5 IS a impact trajectory"
    assert is_buffer12_impact == True, "buffer12_5 IS a impact trajectory"
    assert is_buffer13_impact == True, "buffer13_5 IS a impact trajectory"
    assert is_buffer14_impact == True, "buffer14_5 IS a impact trajectory"
    assert is_buffer15_impact == True, "buffer15_5 IS a impact trajectory"
    assert is_buffer16_impact == True, "buffer16_5 IS a impact trajectory"
    assert is_buffer17_impact == True, "buffer17_5 IS a impact trajectory"
    assert is_buffer18_impact == True, "buffer18_5 IS a impact trajectory"


def generate_points(speed, m, collide, towards, num_points=10, collision_trajectory_threshold=0.05):
    # speed should be given in m/s
    # collide should be True or False: If true... path should be a dangerous collision
    current_time = 0
    time_step = 0.1

    delta_y = speed*time_step*math.sqrt(1/(1+math.pow(m, 2)))
    delta_z = speed*m*time_step*math.sqrt(1/(1+math.pow(m, 2)))

    if collide:
        b = 0.04*math.sqrt(math.pow(m, 2) + 1)
    else:
        b = 0.06*math.sqrt(math.pow(m, 2) + 1)

    y_point = 50
    z_point = m*y_point + b

    data_buffer = []

    for i in range(num_points):
        item = {
                'timestamp': current_time,
                'value': (y_point, z_point)
            }
        data_buffer.append(item)
        y_point += delta_y
        z_point += delta_z
        current_time += time_step
        
    if towards:
        data_buffer.reverse()
    return data_buffer


def run_test():
    #generate_points(2, 2, True, True)
    print("Running trajectory estimation tests...")
    test_trajectory_estimation()
    test_impact()